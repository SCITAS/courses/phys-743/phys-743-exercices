#ifndef GRID_HH
#define GRID_HH

/* -------------------------------------------------------------------------- */
#include <vector>
#include <mpi.h>
#include <array>
/* -------------------------------------------------------------------------- */

class Grid {
public:
  Grid(int m = 0, int n = 0, MPI_Comm communicator = MPI_COMM_WORLD);

  /// access the value [i][j] of the grid
  inline float & operator()(int i, int j) { return m_storage[i * m_n + j]; }
  inline const float & operator()(int i, int j) const {
    return m_storage[i * m_n + j];
  }

  void resize(int m, int n);

  void start_all_communications() {
    MPI_Startall(4, m_requests.data());
  }

  void wait_recv() {
    MPI_Waitall(2, m_requests.data(), MPI_STATUS_IGNORE);
  }

  void wait_send() {
    MPI_Waitall(2, m_requests.data() + 2, MPI_STATUS_IGNORE);
  }

  void init_communications();

  /// set the grid to 0
  void clear();

  int m() const;
  int n() const;

private:
  int m_m, m_n;
  std::vector<float> m_storage;
  MPI_Comm m_communicator;

  /// local neighbors
  int m_north_prank, m_south_prank;

  /// proc rank
  int m_prank;

  /// communicator size
  int m_psize;

  /// request for asynchronous communications
  std::array<MPI_Request, 4> m_requests;
};

#endif /* GRID_HH */
