#ifndef DOUBLE_BUFFER
#define DOUBLE_BUFFER

/* -------------------------------------------------------------------------- */
#include <memory>
#include <mpi.h>
/* -------------------------------------------------------------------------- */
#include "grid.hh"
/* -------------------------------------------------------------------------- */

class DoubleBuffer {
public:
  DoubleBuffer(int m = 0 , int n = 0, MPI_Comm communicator = MPI_COMM_WORLD);

  void resize(int m, int n);
  Grid & current();
  Grid & old();

  void swap();
private:

  std::unique_ptr<Grid> m_current;
  std::unique_ptr<Grid> m_old;
};

#endif /* DOUBLE_BUFFER */
