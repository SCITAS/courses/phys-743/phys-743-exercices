/* -------------------------------------------------------------------------- */
#include "grid.hh"
/* -------------------------------------------------------------------------- */
#include <algorithm>
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
Grid::Grid(int m, int n, MPI_Comm communicator) :
  m_m(m), m_n(n), m_storage(m * n), m_communicator(communicator) {
  clear();

  // retrieving the number of proc and the rank in the proc pool
  MPI_Comm_rank(m_communicator, &m_prank);
  MPI_Comm_size(m_communicator, &m_psize);

  // determining the rank of the neighbors
  m_north_prank = (m_prank == 0 ? MPI_PROC_NULL : m_prank - 1);
  m_south_prank = (m_prank == (m_psize - 1) ? MPI_PROC_NULL : m_prank + 1);
}

/* -------------------------------------------------------------------------- */
void Grid::clear() { std::fill(m_storage.begin(), m_storage.end(), 0.); }

/* -------------------------------------------------------------------------- */
void Grid::resize(int m, int n) {
  m_m = m;
  m_n = n;
  m_storage.resize(m*n);
}

void Grid::init_communications() {
  // Posting the receives requests
  MPI_Recv_init(&(*this)(0, 0), m_n, MPI_FLOAT, m_north_prank, 0,
            m_communicator, &m_requests[0]);
  MPI_Recv_init(&(*this)(m_m - 1, 0), m_n, MPI_FLOAT, m_south_prank, 0,
           m_communicator, &m_requests[1]);

  // posting send requests
  MPI_Send_init(&(*this)(1, 0), m_n, MPI_FLOAT, m_north_prank, 0,
            m_communicator, &m_requests[2]);
  MPI_Send_init(&(*this)(m_m - 2, 0), m_n, MPI_FLOAT, m_south_prank, 0,
            m_communicator, &m_requests[3]);
}

/* -------------------------------------------------------------------------- */
int Grid::m() const { return m_m; }
int Grid::n() const { return m_n; }

/* -------------------------------------------------------------------------- */
