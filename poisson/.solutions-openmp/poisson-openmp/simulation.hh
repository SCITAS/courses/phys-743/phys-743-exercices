#ifndef SIMULATION_HH
#define SIMULATION_HH

/* -------------------------------------------------------------------------- */
#include "double_buffer.hh"
#include "dumpers.hh"
#include "grid.hh"
/* -------------------------------------------------------------------------- */
#include <tuple>
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
class Simulation {
public:
  Simulation(int m, int n);

  /// set the initial conditions, Dirichlet and source term
  virtual void set_initial_conditions();

  /// perform the simulation
  std::tuple<float, int> compute();

  /// access the precision
  void set_epsilon(float epsilon);
  float epsilon() const;

protected:
  /// compute one step and return an error
  virtual void compute_step();

private:
  /// Current error of the solver
  float l2{0};
  
  /// Global problem size
  int m_global_m{0}, m_global_n{0};

  /// Precision to achieve
  float m_epsilon{0};

  /// grid spacing
  float m_h_m{0};
  float m_h_n{0};

  /// Grids storage
  DoubleBuffer m_grids;

  /// source term
  Grid m_f;

  /// Dumper to use for outputs
  std::unique_ptr<Dumper> m_dumper;

  /// number of omp threads
  int m_n_threads{1};
};

#endif /* SIMULATION_HH */
