/* -------------------------------------------------------------------------- */
#include "simulation.hh"
/* -------------------------------------------------------------------------- */
#include <chrono>
#include <iostream>
#include <tuple>
#include <omp.h>
/* -------------------------------------------------------------------------- */

#define N 2048
#define EPSILON 0.005

int main() {

  Simulation simu(N, N);
  simu.set_epsilon(EPSILON);
  
  auto t1 = omp_get_wtime();
  auto n_threads = omp_get_max_threads();

#pragma omp parallel shared(simu, t1)
  {
    simu.set_initial_conditions();

    float l2;
    int k;
    std::tie(l2, k) = simu.compute();

#pragma omp single nowait 
    std::cout <<"nb steps " << k << " [l2 = " << l2 << "] "
	      << (omp_get_wtime() - t1) << "s on " << n_threads << " threads" << std::endl;
  }
  
  return 0;
}
