#ifndef GRID_HH
#define GRID_HH

/* -------------------------------------------------------------------------- */
#include <array>
#include <vector>
#include <mpi.h>
#include <iostream>
/* -------------------------------------------------------------------------- */
namespace {
struct DEBUG {
#if defined(DEBUG)
  static void print(const std::string & func) {
    std::cerr << func << "\n";
  }
#else
  static void print(const std::string & ) {
  
  }
#endif
};
} // namespace

/* -------------------------------------------------------------------------- */

class Grid {
public:
  Grid(int m = 0, int n = 0);
  ~Grid();

  /// access the value [i][j] of the grid
  inline float & operator()(int i, int j) { return m_storage[i * m_n + j]; }
  inline const float & operator()(int i, int j) const {
    return m_storage[i * m_n + j];
  }

  void resize(int m, int n);

  /// set the grid to 0
  void clear();

  int m() const;
  int n() const;

  void initializeHaloCommunications(MPI_Comm & m_communicator,
                                    const std::array<int, 2> & ghosts);
  void startSynchronization() {
    MPI_Request * requests = m_requests.data();
    MPI_Startall(8, requests);
  }

  void waitReceives() {
    auto requests = m_requests.data();
    MPI_Waitall(4, requests, MPI_STATUS_IGNORE);
  }

  void waitSends() {
    auto requests = m_requests.data();
    MPI_Waitall(4, requests + 4, MPI_STATUS_IGNORE);
  }

private:
  int m_m, m_n;
  std::vector<float> m_storage;

  /// request for asynchronous communications
  std::array<MPI_Request, 8> m_requests;

  /// communication types
  MPI_Datatype column_t, line_t;
};

#endif /* GRID_HH */
