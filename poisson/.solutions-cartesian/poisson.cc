/* -------------------------------------------------------------------------- */
#include "simulation.hh"
/* -------------------------------------------------------------------------- */
#include <chrono>
#include <iostream>
#include <sstream>
#include <tuple>
/* -------------------------------------------------------------------------- */
#include <mpi.h>
/* -------------------------------------------------------------------------- */

#define EPSILON 0.05

typedef std::chrono::high_resolution_clock clk;
typedef std::chrono::duration<double> second;

static void usage(const std::string & prog_name) {
  std::cerr << prog_name << " <grid_size>" << std::endl;
  exit(0);
}

int main(int argc, char * argv[]) {
  int prank{0}, psize{1}, nthreads{1};
  MPI_Init(NULL, NULL);
  // assert(provided == MPI_THREAD_MULTIPLE);

  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);

  if (argc != 2)
    usage(argv[0]);

  std::stringstream args(argv[1]);
  int N;
  args >> N;

  if (args.fail())
    usage(argv[0]);

  std::unique_ptr<Simulation> simu;

  simu = std::make_unique<Simulation>(N, N, MPI_COMM_WORLD);

  simu->set_initial_conditions();
  simu->set_epsilon(EPSILON);

  float l2;
  int k;

  auto start = clk::now();
  std::tie(l2, k) = simu->compute();
  auto end = clk::now();

  second time = end - start;

  auto min = simu->min();
  auto max = simu->max();

  if (prank == 0)
    std::cout << "MPI:   " << psize << " (" << simu->dims(0) << ", " << simu->dims(1) << ")\n"
              << "OMP:   " << nthreads << "\n"
              << "Grid:  " << N << "x" << N << "\n"
              << "Bound: " << "[ " << min << " : " << max << " ]" << "\n"
              << "Iter:  " << k << "\n"
              << "Norm:  " << std::scientific << l2 << "\n"
              << "Time:  " << time.count()  << "s" << "\n"
              << "Flops: " << simu->complexity()*k/time.count()/1e9  << " GFlops" << std::endl;

  simu.release(); // To force destruction of MPI related stuff
  MPI_Finalize();

  return 0;
}
