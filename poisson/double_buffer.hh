#ifndef DOUBLE_BUFFER
#define DOUBLE_BUFFER

/* -------------------------------------------------------------------------- */
#include <memory>
/* -------------------------------------------------------------------------- */
#include "grid.hh"
/* -------------------------------------------------------------------------- */

class DoubleBuffer {
public:
  DoubleBuffer(int m = 0, int n = 0);

  Grid & current();
  Grid & old();

  // resize the grid
  void resize(int m, int n);

  void swap();
private:

  std::unique_ptr<Grid> m_current;
  std::unique_ptr<Grid> m_old;
};

#endif /* DOUBLE_BUFFER */
