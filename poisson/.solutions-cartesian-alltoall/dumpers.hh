#if defined(_MPI)
#include <mpi.h>
#endif

#ifndef DUMPERS_HH
#define DUMPERS_HH

/* -------------------------------------------------------------------------- */
class Grid;

/* -------------------------------------------------------------------------- */
class Dumper {
public:
#if defined(_MPI)
  explicit Dumper(MPI_Comm communicator)
      : m_min(-1.), m_max(1.), m_communicator(communicator) {}
#else
  Dumper() : m_min(-1.), m_max(1.) {}
#endif
  virtual void dump(const Grid & m_grid, int step) = 0;

  void set_min(float min);
  void set_max(float max);

  float min() const;
  float max() const;

protected:
  float m_min, m_max;
#if defined(_MPI)
  MPI_Comm m_communicator;
#endif
};

/* -------------------------------------------------------------------------- */
class DumperASCII : public Dumper {
public:
#if defined(_MPI)
  explicit DumperASCII(MPI_Comm communicator) : Dumper(communicator) {}
#else
  DumperASCII() : Dumper() {}
#endif

  virtual void dump(const Grid & grid, int step);
};

/* -------------------------------------------------------------------------- */
class DumperBinary : public Dumper {
public:
#if defined(_MPI)
  explicit DumperBinary(MPI_Comm communicator) : Dumper(communicator) {}
#else
  DumperBinary() : Dumper() {}
#endif
  virtual void dump(const Grid & grid, int step);
};

#endif /* DUMPERS_HH */
