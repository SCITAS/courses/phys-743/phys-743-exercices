#ifndef GRID_HH
#define GRID_HH

/* -------------------------------------------------------------------------- */
#include <array>
#include <vector>
#include <mpi.h>
#include <iostream>
/* -------------------------------------------------------------------------- */
namespace {
struct DEBUG {
#if defined(DEBUG)
  static void print(const std::string & func) {
    std::cerr << func << "\n";
  }
#else
  static void print(const std::string & ) {
  
  }
#endif
};
} // namespace

/* -------------------------------------------------------------------------- */

class Grid {
public:
  Grid(int m = 0, int n = 0);
  ~Grid();

  /// access the value [i][j] of the grid
  inline float & operator()(int i, int j) { return m_storage[i * m_n + j]; }
  inline const float & operator()(int i, int j) const {
    return m_storage[i * m_n + j];
  }

  void resize(int m, int n);

  /// set the grid to 0
  void clear();

  int m() const;
  int n() const;

private:
  int m_m, m_n;
  std::vector<float> m_storage;

  /// request for asynchronous communications
  std::array<MPI_Request, 8> m_requests;
};

#endif /* GRID_HH */
