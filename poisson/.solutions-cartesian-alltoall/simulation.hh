#ifndef SIMULATION_HH
#define SIMULATION_HH

/* -------------------------------------------------------------------------- */
#include "double_buffer.hh"
#include "dumpers.hh"
#include "grid.hh"
/* -------------------------------------------------------------------------- */
#include <array>
#include <memory>
#include <tuple>
#include <mpi.h>
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
class Simulation {
public:
  Simulation(int m, int n, MPI_Comm communicator);

  /// set the initial conditions, Dirichlet and source term
  virtual void set_initial_conditions();

  /// perform the simulation
  std::tuple<float, int> compute();

  /// access the precision
  void set_epsilon(float epsilon);
  float epsilon() const;

  std::size_t complexity() { return 8 * m_global_size[0] * m_global_size[1]; }

  float min() const;
  float max() const;

  float dims(std::size_t i) const { return m_dims[i]; }

protected:
  /// compute one step and return an error
  virtual float compute_step();

  /// compute one row
  float compute_row(int i);

private:
  /// Global problem size
  std::array<int, 2> m_global_size;

  /// Local problem size
  std::array<int, 2> m_local_size;

  /// local offset
  std::array<int, 2> m_offset;

  /// Cartesian dimensions
  std::array<int, 2> m_dims;

  /// Cartesian coordinates
  std::array<int, 2> m_coords;

  /// proc rank
  int m_prank{0};

  /// communicator size
  int m_psize{1};

  /// Precision to achieve
  float m_epsilon;

  /// grid spacing
  std::array<float, 2> m_h;

  /// Grids storage
  DoubleBuffer m_grids;

  /// source term
  Grid m_f;

  /// Dumper to use for outputs
  std::unique_ptr<Dumper> m_dumper;

  /// communicator
  MPI_Comm m_communicator;

  std::array<MPI_Aint, 4> m_sdispls, m_rdispls;
  std::array<MPI_Datatype, 4> m_types;
  std::array<int, 4> m_counts;

  MPI_Datatype m_line_t, m_column_t;
};

#endif /* SIMULATION_HH */
