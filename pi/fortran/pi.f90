program pi_prog
  use iso_fortran_env, only: real64, int64

  implicit none

  integer, parameter :: dwp = real64
  integer, parameter :: iwp = int64

  integer, parameter :: N = 10000000

  integer(iwp) :: clock_rate, start, finish
  real(dwp) :: total_time

  real(dwp) :: dx, x, sum, pi
  integer :: i

  ! Prepare system clock
  call system_clock(count_rate=clock_rate)

  call system_clock(start)
  dx = 1.0 / N
  sum = 0.0
  do i = 0, N-1
    x = i * dx
    sum = sum + f(x)
  end do
  pi = dx * sum
  call system_clock(finish)
  total_time = real(finish - start) / real(clock_rate)

  print*, "Computed pi = ", pi
  print*, "Elapsed time = ", total_time, "s"

contains

  real(dwp) pure function f(a)
    real(dwp), intent(in) :: a
    f = 4.0 / (1.0 + a*a)
  end function f
end program pi_prog
