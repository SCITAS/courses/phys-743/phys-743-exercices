/*
  This exercise is taken from the class Parallel Programming Workshop (MPI,
  OpenMP and Advanced Topics) at HLRS given by Rolf Rabenseifner
 */
#include <chrono>
#include <cstdio>
#include <cmath>
#include <numeric>
#include <vector>
#include <mpi.h>

using clk = std::chrono::high_resolution_clock;
using second = std::chrono::duration<double>;
using time_point = std::chrono::time_point<clk>;

inline int digit(double x, int n) {
  return std::trunc(x * std::pow(10., n)) - std::trunc(x * std::pow(10., n - 1)) *10.;
}

inline double f(double a) { return (4. / (1. + a * a)); }

const int n = 10000000;

int main(int /* argc */ , char ** /* argv */) {
  int i;
  double dx, x, sum, pi;

  MPI_Init(NULL, NULL);
  int prank, psize;
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);
  auto t1 = clk::now();

  auto ln = n/psize + (prank < n % psize ? 1 : 0);
  
  auto i_start = prank * ln + (prank < n % psize ? 0 : n % psize);
  auto i_end = i_start + ln; 
  
  /* calculate pi = integral [0..1] 4 / (1 + x**2) dx */
  dx = 1. / n;
  double lsum = 0.0;
  for (i = i_start; i < i_end; i++) {
    x = 1. * i * dx;
    lsum = lsum + f(x);
  }

  std::vector<double> partial_sums;
  if (prank == 0) {
    partial_sums.resize(psize);
  }

  MPI_Gather(&lsum, 1, MPI_DOUBLE, partial_sums.data(), 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  if (prank == 0) {
    sum = std::accumulate(partial_sums.begin(), partial_sums.end(), 0);
  }

  pi = dx * sum;
  
  MPI_Bcast(&sum, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  
  second elapsed = clk::now() - t1;

  if (prank == 0) {
    std::printf("computed pi                     = %.16g\n", pi);
    std::printf("wall clock time (chrono)        = %.4gs\n", elapsed.count());
    std::printf("n mpi process                   = %.d\n", psize);
  

    for(int d = 1; d <= 15; ++d) {
      std::printf("%d", digit(pi, d));
    }
  }
  
  MPI_Finalize();
  
  return 0;
}
