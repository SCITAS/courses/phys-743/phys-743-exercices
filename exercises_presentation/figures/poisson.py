import numpy as np
import matplotlib.pyplot as plt


N = 1000
dx = 1 / N

x, y = np.mgrid[slice(0, 1, dx), slice(0, 1, dx)]
z = -200.0 * np.pi**2 * np.sin(10.0 * np.pi * x) * np.sin(10.0 * np.pi * y)

fig, ax = plt.subplots()
p = ax.pcolormesh(x, y, z)

ax.set_xlabel('x')
ax.set_ylabel('y')

plt.show()

fig.savefig('poisson.png')
