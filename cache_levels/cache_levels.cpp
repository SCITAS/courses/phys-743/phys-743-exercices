/*
  Compute the number of copy operations per second done as a function of the array size

  This program execute a copy of an array and repeats the operation REPEAT times for better timings.
  */
#include <array>
#include <iostream>
#include <chrono>

using clk = std::chrono::high_resolution_clock;
using second = std::chrono::duration<double>;

#ifndef N
#  define N 256
#endif

#ifndef REP
#  define REP 1000
#endif

const int REPEAT = REP;
const size_t SIZE = N;

// Dummy routine put there to trick the compiler thinking we really need to compute the b array. Otherwise, it
// will optimize out the loop computing b.
void dummy(std::array<int, SIZE> b) {
    if (b[0] < 0) {
        std::cout <<"Never happening";
    }
}

int main() {
  std::array<int, SIZE> a, b;

  for (auto i = 0; i < SIZE; i++) {
      a[i] = 2*i;
  }

  auto start = clk::now();
  for (auto r = 1; r < REPEAT; r++) {
    for (auto i = 0; i < SIZE; i++) {
      b[i] = a[i];
    }
    dummy(b);
  }
  second elapsed = (clk::now() - start);

  std::cout <<"Size of the arrays = " <<SIZE * 4 / 1024 <<"kB" <<std::endl; // We store 4-byte integers into the arrays
  std::cout <<"Number of operations per second = " <<REPEAT * SIZE / elapsed.count() <<std::endl;
}
