program cache_level
  use iso_fortran_env, only: real64, int64

  implicit none

#ifndef N
#define N 256
#endif

#ifndef REP
#define REP 256
#endif

  integer, parameter :: dwp = real64
  integer, parameter :: iwp = int64

  integer, parameter :: SIZE = N
  integer, parameter :: REPEAT = REP

  integer :: r, i
  integer(iwp) :: clock_rate, start, finish
  real(dwp) :: total_time
  integer, dimension(SIZE) :: a, b

  ! Prepare system clock
  call system_clock(count_rate=clock_rate)

  do i = 1, SIZE
    A(i) = 2 * i
  end do

  call system_clock(start)
  do r = 1, REPEAT
    do i = 1, SIZE
      B(i) = A(i)
    end do
    call dummy(b)
  end do
  call system_clock(finish)
  total_time = (finish - start) / real(clock_rate, dwp)

  print*, "Size = ", SIZE * 4 / 1024, "kB, total time = ", total_time
  print*, "Operations per second = ", REPEAT * SIZE / total_time

contains

  subroutine dummy(arr)
    integer, intent(in) :: arr(:)

    if (arr(1) < 0) then
      print*, "Never happening"
    end if
  end subroutine dummy
end program cache_level
