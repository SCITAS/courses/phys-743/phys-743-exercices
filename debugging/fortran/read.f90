program read
  implicit none

  integer, parameter :: N = 1000
  real :: data(N)

  integer :: i
  real :: sum

  do i = 1, N
    data(i) = i
  end do

  sum = 0.0
  do i = 1, N+1
    sum = sum + data(i)
  end do

  print*, (N * (N-1)) / 2.0, " == ", sum
end program read
